#!/bin/bash

apt install zsh curl git -y

chsh -s $(which zsh)

sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended

curl -sS https://starship.rs/install.sh | sh -s -- -y

git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions

git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting

sed -i.bak '/^plugins/c\plugins=(\ngit\nzsh-autosuggestions\nzsh-syntax-highlighting\ncolored-man-pages\ncommand-not-found\n)' ~/.zshrc

echo 'eval "$(starship init zsh)"' >> ~/.zshrc

[[ -d /usr/local/bin/ ]] && echo "OK. Reload your shell with source ~/.zshrc or reboot system" || (mv /usr/local/bin/starship /usr/bin && echo "Bin moved. Reload your shell with source ~/.zshrc or reboot system")

[[ $(systemd-detect-virt) =~ ^kvm* ]] && echo "Done." || (mkdir -p ~/.config && echo -e "[container]\ndisabled = true" >> ~/.config.starship.toml)
