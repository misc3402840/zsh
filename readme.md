# ZSH

##### Install zsh

```bash
apt install zsh curl git -y
```

##### Makie it your default shell

```bash
chsh -s $(which zsh)
```

##### Install Oh-My-Zsh

```bash
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended
```

##### Install starship

```bash
curl -sS https://starship.rs/install.sh | sh -s -- -y
```

##### Install plugins

```bash
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions && \
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
```

##### Setup shell to use starship

Backup config file and edit:

```bash
sed -i.bak '/^plugins/c\plugins=(\ngit\nzsh-autosuggestions\nzsh-syntax-highlighting\ncolored-man-pages\ncommand-not-found\n)' ~/.zshrc
```

append line to config:

```bash
echo 'eval "$(starship init zsh)"' >> ~/.zshrc
```

Configure starhip while using LXC:

```bash
[[ $(systemd-detect-virt) =~ ^kvm* ]] && echo "Done." || (mkdir -p ~/.config && echo -e "[container]\ndisabled = true" >> ~/.config.starship.toml)
```

Setup bin path

> If during installation scritp installed bin into default destionation `/usr/local/bin/` which is not in your path, move it accordingly:

```bash
[[ -d /usr/local/bin/ ]] && echo "OK. Reload your shell with `source ~/.zshrc` or reboot system" || (mv /usr/local/bin/starship /usr/bin && echo "Bin moved. Reload your shell with `source ~/.zshrc` or reboot system")
```

